import pygame
import os
import sys

#Constants
pygame.init()
black = (0,0,0)
white = (255,255,255)
blue = (19,19,197)
yellow = (255,238,0)
height = 0
myfont = pygame.font.SysFont("monospacebold", 50)
mazefile = open('Maze.txt','r')

#set width and height
for line in mazefile:
    line.strip('\n')
    width = len(line)
    height += 1

#load images
energizer = pygame.image.load('./Images/Energizer.png')
dot = pygame.image.load('./Images/Dot.png')

#lists
squaresnoteaten = []
squareseaten = []
edges = []
teleport = []
allspaces = []

class PacmanClass(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image_open = pygame.image.load('./Images/Pacman open.png')
        self.image_closed = pygame.image.load('./Images/Pacman closed.png')
        self.index = (19*12) + 9
        self.direction = 'none'
        self.display_image_open = self.image_open
        self.display_image_closed = self.image_closed
        self.state = 'open'
        self.newindex = self.index

    def update_direction(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP] and allspaces[self.index - 19] not in edges:
            self.direction = 'up'
        if keys[pygame.K_DOWN] and allspaces[self.index + 19] not in edges:
            self.direction = 'down'
        if keys[pygame.K_LEFT] and allspaces[self.index - 1] not in edges:
            self.direction = 'left'
        if keys[pygame.K_RIGHT] and allspaces[self.index + 1] not in edges:
            self.direction = 'right'

    def update_location(self):
        if self.direction == 'up':
            self.newindex = self.index - 19
        if self.direction == 'down':
            self.newindex = self.index + 19
        if self.direction == 'left':
            self.newindex = self.index - 1
            if allspaces[self.index] == teleport[0]:
                self.newindex = allspaces.index(teleport[1])
        if self.direction == 'right':
            self.newindex = self.index + 1
            if allspaces[self.index] == teleport[1]:
                self.newindex = allspaces.index(teleport[0])
        if allspaces[self.newindex] not in edges:
            self.index = self.newindex
            
    def get_orientation(self):
        if self.direction == 'up':
            self.display_image_open = pygame.transform.rotate(self.image_open,90)
            self.display_image_closed = pygame.transform.rotate(self.image_closed,90)
        if self.direction == 'down':
            self.display_image_open = pygame.transform.rotate(self.image_open,270)
            self.display_image_closed = pygame.transform.rotate(self.image_closed,270)
        if self.direction == 'left':
            self.display_image_open = pygame.transform.flip(self.image_open,True,False)
            self.display_image_closed = pygame.transform.flip(self.image_closed,True,False)
        if self.direction == 'right':
            self.display_image_open = self.image_open
            self.display_image_closed = self.image_closed
            
    def animation(self):
        self.get_orientation()
        self.update_location()
        if self.state == 'open':
            self.state = 'closed'
            return(self.display_image_open)
        else:
            self.state = 'open'
            return(self.display_image_closed)
            

class GhostClass(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image1 = pygame.image.load('./Images/Red ghost right.png')
        self.image2 = pygame.image.load('./Images/Red ghost right 2.png')
        self.image_num = 0
        self.index = (19 * 8) + 9
        self.magnitude = 0
        self.horizontal = ''
        self.verticle = ''
        self.amountMoved = 0
        self.temp = 0

    def animation(self):
        if self.image_num == 0:
            self.image_num = 1
            return(self.image1)
        else:
            self.image_num = 0
            return(self.image2)

    def move(self,pacman):
        if self.amountMoved <= 5:
            path = self.Astar(pacman)
            self.index = path[len(path)-(3+self.temp)]
            self.amountMoved += 1
        else:
            self.amountMoved = 0

    def Astar(self,pacman):
        openlist = []
        openlistf = []
        closedlist = []
        closedlistf = []
        startnode = Node(0,0,self.index,0)
        endnode = Node(0,0,pacman.index,0)
        openlist.append(startnode)
        openlistf.append(startnode.f)
        while openlist != []:
            q = openlist[openlistf.index(min(openlistf))]
            openlist.remove(q)
            openlistf.remove(min(openlistf))
            if allspaces[q.index] == teleport[0]:
                successors = [q.index + 1, allspaces.index(teleport[1])]
            elif allspaces[q.index] == teleport[1]:
                successors = [q.index - 1, allspaces.index(teleport[0])]
            else:
                successors = [q.index + 1, q.index - 1, q.index + 19, q.index - 19]
            for successor in successors:
                if successor == endnode.index:
                    path = [q.index]
                    self.pathcalc(path,q)
                    path.append(endnode.index)
                    return path                    
                elif allspaces[successor] in edges:
                    pass
                else:
                    g = q.g + 1
                    h = ((endnode.index // 19 - successor // 19)**2 + (endnode.index % 19 - successor % 19)**2)** 0.5
                    successornode = Node(g,h,successor,q)
                    if successornode in openlist:
                        if openlistf[openlist.index(successornode)] < successornode.f:
                            pass
                    elif successornode in closedlist:
                        if closedlistf[closedlist.index(successornode)] < successornode.f:
                            pass
                    else:
                        openlist.append(successornode)
                        openlistf.append(successornode.f)
            closedlist.append(q)
            closedlistf.append(q.f)
            
    def pathcalc(self,path,node):
        done = False
        if node.parent != 0:
            path.append(node.parent.index)
        else:
            done = True
        if done != True:
            self.pathcalc(path,node.parent)
    
class Node:
    def __init__(self,g,h,index,parent):
        self.parent = parent
        self.index = index
        self.g = g
        self.h = h
        self.f = self.g + self.g
                    
            
#Gui class
class GUIClass:
    #constructor method to define the variable specific to the GUI class
    def __init__(self):
        self.tilemap = open('Maze.txt','r')
        self.width = width * 35
        self.height = height * 35
        self.gameDisplay = pygame.display.set_mode((self.width,self.height))
        self.waittime = 0
        self.num_energizers_eaten = 0
        self.backuplist = []
        self.energizers = []
        self.score = 0

    #method to set up the screen 
    def screen_setup(self):
        x = -1
        y = -1
        for line in self.tilemap:
            line.strip('\n')
            y += 1
            x = -1
            for letter in line:
                x += 1
                if x != 19:
                    rect = pygame.Rect(x*35, y*35, 35, 35)
                    if letter == 'W' or letter == 'w':
                        edges.append(rect)
                    elif letter == 'T' or letter == 't':
                        teleport.append(rect)
                        squaresnoteaten.append(rect)
                    elif letter == 'E':
                        self.energizers.append(rect)
                    elif letter == 'N':
                        squareseaten.append(rect)
                    elif letter == 'S':
                        pass
                    else:
                        squaresnoteaten.append(rect)
                    allspaces.append(rect)
        pygame.display.set_caption('Maths Pacman')
        self.tilemap.close()

    #method to update the screen after every key press    
    def update_screen(self,pacman,ghost):
        if allspaces[pacman.index] in squaresnoteaten:
            self.score += 100
            squareseaten.append(allspaces[pacman.index])
            squaresnoteaten.remove(allspaces[pacman.index])
        if len(self.backuplist) == 4:
            self.energizers = self.backuplist
            self.backuplist = []
        if allspaces[pacman.index] in self.energizers:
            self.waittime += 1
            self.num_energizers_eaten += 1
            if self.num_energizers_eaten <= 4:
                self.score += 200
            self.backuplist.append(allspaces[pacman.index])
            self.energizers.remove(allspaces[pacman.index])
        self.gameDisplay.fill(black)
        for item in edges:
            pygame.draw.rect(self.gameDisplay, blue, item)
        for item in squaresnoteaten:
            self.gameDisplay.blit(dot,item)
        for item in self.energizers:
            self.gameDisplay.blit(energizer,item)
        label = myfont.render("Score: "+ str(self.score), 1, yellow)
        self.gameDisplay.blit(label, allspaces[0])
        self.gameDisplay.blit(pacman.animation(),allspaces[pacman.index])
        if pygame.time.get_ticks() > 1000:
            ghost.move(pacman)
        self.gameDisplay.blit(ghost.animation(),allspaces[ghost.index])
        pygame.display.update()

    def CollisionCheck(self,pacman,ghost):
        if pacman.index == ghost.index:
            return(True)

    def endscreen(self):
        self.gameDisplay.fill(white)
        label = myfont.render("You Lose", 1, black)
        self.gameDisplay.blit(label, allspaces[0])
        label = myfont.render("Score: " + str(self.score), 1, black)
        self.gameDisplay.blit(label, allspaces[19])
        pygame.display.update()
        
GUI = GUIClass()
pacman = PacmanClass()
Redghost = GhostClass()
GUI.screen_setup()
crashed = False
while not crashed:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            crashed = True
    pacman.update_direction()
    GUI.update_screen(pacman,Redghost)
    crashed = GUI.CollisionCheck(pacman,Redghost)
    pygame.time.wait(90)
GUI.endscreen()
    
